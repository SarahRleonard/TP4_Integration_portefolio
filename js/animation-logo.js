var animation = anime({
    targets: '.tick-demo .el',
    translateX: 270,
    direction: 'alternate',
    loop: true,
    easing: 'easeInOutQuad',
    autoplay: false
});

function loop(t) {
    animation.tick(t);
    customRAF = requestAnimationFrame(loop);
}

requestAnimationFrame(loop);